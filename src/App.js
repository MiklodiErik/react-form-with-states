import { useState } from "react";
import { Form, Button, ProgressBar } from "react-bootstrap";

export default function App() {
  return (
    <div className="App">
      <LoginForm />
    </div>
  );
}

function LoginForm() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordStrength, setPasswordStrength] = useState("not accepted");

  function handleEmailChange(email) {
    const EMAIL_PATTERN = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    EMAIL_PATTERN.test(email) ? setEmail(email) : setEmail("");
  }
  function handlePasswordChange(password) {
    const WEAK_PATTERN = /^(?=.*[A-Z])(?=.*\d)(?=.*[\W_]).{8,}/;
    const STRONG_PATTERN =
      /^(?=(?:.*[A-Z]){2})(?=(?:.*\d){2})(?=(?:.*[\W_]){2}).{14,}$/;

    if (STRONG_PATTERN.test(password) && WEAK_PATTERN.test(password)) {
      setPasswordStrength("strong");
      setPassword(password);
    }
    if (!STRONG_PATTERN.test(password) && WEAK_PATTERN.test(password)) {
      setPasswordStrength("weak");
      setPassword(password);
    }
    if (!STRONG_PATTERN.test(password) && !WEAK_PATTERN.test(password)) {
      setPasswordStrength("not accepted");
      setPassword("");
    }
  }

  function handleSubmit() {
    if (email === "" || password === "") return;

    const newUser = { email, password };
    console.log(newUser);
  }

  return (
    <form className="container mt-5">
      <div>
        <Form.Label htmlFor="email">Email</Form.Label>
        <Form.Control
          type="email"
          id="email"
          className={email === "" ? "is-invalid" : "is-valid"}
          onChange={(e) => handleEmailChange(e.target.value)}
        />
        <div className="invalid-feedback">
          Please enter a valid email address
        </div>
      </div>
      <div className="mt-3 mb-2">
        <Form.Label htmlFor="password">Password</Form.Label>
        <Form.Control
          type="text"
          id="password"
          className={password === "" ? "is-invalid" : "is-valid"}
          onChange={(e) => handlePasswordChange(e.target.value)}
        />
        <div className="invalid-feedback">
          At least 8 characters, 1 capital letter, 1 number and 1 special
          character
        </div>
      </div>

      <StrengthMeter passwordStrength={passwordStrength} />

      <div className="d-grid mt-4">
        <Button
          className={`btn-lg ${
            email !== "" && password !== ""
              ? "btn-success"
              : "disabled btn-danger"
          }`}
          onClick={handleSubmit}
        >
          Register Account
        </Button>
      </div>
    </form>
  );
}

function StrengthMeter({ passwordStrength }) {
  const strengthMeterDerivedParams = {
    "not accepted": {
      percent: 0,
      color: "warning",
    },
    weak: {
      percent: 50,
      color: "warning",
    },
    strong: {
      percent: 100,
      color: "success",
    },
  };

  //console.log(strengthMeterDerivedParams);

  return (
    <ProgressBar
      now={strengthMeterDerivedParams[passwordStrength].percent}
      label={passwordStrength}
      variant={strengthMeterDerivedParams[passwordStrength].color}
    />
  );
}
